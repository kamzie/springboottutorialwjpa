package zielware.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdenDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdenDataApplication.class, args);
	}

}

