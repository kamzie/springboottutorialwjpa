package zielware.com.course;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course, String> {

	//public List<Course> getCourseByName(String topicId);
	//name should fallow the rules:
	//1 find
	//2 by (filter by particular condition)...
	//3 name
	//public List<Course> findByName(String name);
	//You dont have to imlement the method, just declare the method
	//with the findByPROPERTY name format and Spring will implement it for you
	//Write method names by camel case
	public List<Course> findByTopicId(String topicId);

}