package zielware.com.topic;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicController {

	//autowire annotation marks this as something that needs dependency injection
	@Autowired
	private TopicService topicService;
	
	//Spring MVC maps this request, executes this method and 
	//returned object is converted to JSON and send back as http response
	//Generated JSON has key names corresponding to property names of the Topic class.
	//The JSON values are the values of those properties
	//Default Request method is GET
	@RequestMapping("/topics")
	public List<Topic> getAllTopics() {
		return topicService.getAllTopics();
	}
	
	//it how to map this method and id part
	//@RequestMapping("/topics/spring") //only for spring
	@RequestMapping("/topics/{id}")
	public Topic getTopic(@PathVariable String id) {
		return topicService.getTopic(id);
	}	
	
	//map this method to any request that`s a post on topics 
	@RequestMapping(method=RequestMethod.POST, value="/topics")
	public void addTopic(@RequestBody Topic topic) {
		topicService.addTopic(topic);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/topics/{id}")
	public void updateTopic(@RequestBody Topic topic, @PathVariable String id) {
		topicService.updateTopic(id, topic);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/topics/{id}")
	public void delTopic(@PathVariable String id) {
		topicService.delTopic(id);
	}	
}
