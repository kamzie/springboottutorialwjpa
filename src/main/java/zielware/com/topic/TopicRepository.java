package zielware.com.topic;

import org.springframework.data.repository.CrudRepository;

//dont need to create class, because spring provide class,
//
//public class TopicRepository {
public interface TopicRepository extends CrudRepository<Topic, String> {
	
	//Common method for every entity in JPA, they`re created OoB
	//getAllTopics()
	//getTopic(String id)
	//updateTopic(Topic t)
	//deleteTopic(String id)
	
}