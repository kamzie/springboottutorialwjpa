package zielware.com.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 * business service is typically singleton
 * When app starts spring create and keep in memory instance of these class
 * and there may be a lot of controllers which depend of this
 * Service annotation marks this class as a business service
 */

@Service
public class TopicService {
	
	@Autowired
	private TopicRepository topicRepository;

	public List<Topic> getAllTopics() {
		//return topics;
		List<Topic> topics = new ArrayList<>();
		//:: is method reerence from lambda basics
		topicRepository.findAll().forEach(topics::add);	
		return topics;
	}

	public Topic getTopic(String id) {
		//return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
		Topic t = topicRepository.findById(id).get();
		return t;
	}

	public void addTopic(Topic topic) {
		//topics.add(topic);
		topicRepository.save(topic);
	}

	public void updateTopic(String id, Topic topic) {
		/*
		for(Topic t : topics) {
			if(id.equals(t.getId())){
				t.setId(topic.getId());
				t.setDescription(topic.getDescription());
				t.setName(topic.getName());
				return;
			}
		}*/
		topicRepository.save(topic);
	}

	public void delTopic(String id) {
		//topics.removeIf(t -> t.getId().equals(id));
		topicRepository.deleteById(id);
	}
}
