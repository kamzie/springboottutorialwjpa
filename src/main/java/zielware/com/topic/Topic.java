package zielware.com.topic;

import javax.persistence.Entity;
import javax.persistence.Id;

//topic model object
// it tells JPA that it needs to be save in DB 
@Entity
public class Topic {

	//table Topic will have 3 columns...
	@Id //marks as primary column
	private String id;
	private String name;
	private String description;
	

	public Topic() {
		
	}
	
	public Topic(String id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
